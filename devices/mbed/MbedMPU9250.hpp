#pragma once

#ifdef __MBED__

#include "../MPU9250.hpp"
#include <memory>
#include <mbed.h>

namespace robofish::edc {

struct MbedMPU9250 final : public MPU9250
{
	MbedMPU9250(mbed::InterruptIn& interruptPin, mbed::I2C& i2c, MPU9250::I2CAddress i2cAddress = MPU9250::I2CAddress::AD0_Low);
	~MbedMPU9250();

private:
	mbed::I2C& _i2c;
	mbed::InterruptIn& _irq;

	bool _writeSingleByte(MPU9250::Register targetRegister, int value);
	bool _readSingleByte(MPU9250::Register targetRegister, int& value);
	bool _readDoubleByte(MPU9250::Register startRegister, int& value);
	void _enableIRQ();
	void _disableIRQ();
	void _wait(int milliseconds);
};

} // namespace robofish::edc

#endif // __MBED__
