#pragma once
#include "../Logger.hpp"
#include <cstdint>

#ifdef __MBED__

namespace robofish::edc {

struct MbedLogger : public Logger
{
	/**
	 * Call this before first use.
	 */
	static void initialize(uint32_t baud_rate = 115200);

	/**
	 * The bare-metal Mbed configuration does not support floating point formatting in printf calls.
	 * Workaround: Separating floating point numbers into their whole and fractional parts and printing
	 * out two integers separated by a decimal point character.
	 */
	void write(const char * message, ...);

	void printSystemInfo();
};

} // namespace robofish::edc

#endif // __MBED__
