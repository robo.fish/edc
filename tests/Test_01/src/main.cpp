#include <mbed.h>
#include <stdarg.h>
#include <PinNames.h>
#include "utils/mbed/MbedLogger.hpp"
#include "devices/mbed/MbedDOGM204.hpp"
#include "utils/Initializer.hpp"

using namespace robofish::edc;

DigitalOut led1(LED1);
mbed::I2C i2c(I2C_SDA, I2C_SCL);

#if defined(TARGET_NUCLEO_F411RE) || defined(TARGET_NUCLEO_L432KC)
DigitalOut displayResetPin(PA_8, 1);
#else
#pragma GCC error "You need to provide the reset pin for the display."
#endif
MbedDOGM204 display(displayResetPin, i2c);

void setup();
void loop();
static int32_t loopCounter = 0;

int main()
{
	setup();
	while (true)
	{
		loop();
		++loopCounter;
		rtos::ThisThread::sleep_for(100ms);
	}
}

void setup()
{
	MbedLogger::initialize();
	MbedLogger logger;
	logger.printSystemInfo();

	i2c.frequency(400000);

	if (Initializer(logger).testedInitialization(display, "DOGM204-A display"))
	{
		display.write("init");
	}
}

static char textBuffer[2] = { 'A', 0x00 };

void loop()
{
	if (loopCounter % 5 == 0)
	{
		led1 = !led1;
	}

	if (loopCounter % 10 == 0)
	{
		textBuffer[0] = ((loopCounter/10) % 26);
		display.write(textBuffer);
	}
}
