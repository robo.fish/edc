#pragma once
#include <cstdint>
#include <cstdlib>
#include "../utils/Initializer.hpp"

namespace robofish::edc {

struct DOGM204 : public Initializable, Testable
{
	enum class I2CAddress : int
	{
		SA0_Low  = 0x78,
		SA0_High = 0x7A
	};

	enum ControlByte : int
	{
		OneCommand  = 0x80,  // the following byte is a command
		OneData     = 0xC0,  // the following byte is data to be displayed
		AllData     = 0x40   // all following bytes are data to be displayed
	};

	/// The values for selected command byte values that are sent after the control byte.
	/// For the I2C mode, the pin RS(SA0) is hard-wired to 0 in our hardware setup.
	enum LCDCommand : int
	{
		ClearDisplay = 0x01,

		// extended function bit on (RE=1)
		// IS=0
		// display configured 8-bit data length (DL=1)
		// four-line or two-line display (N=1)
		// reverse display off (REV=0)
		ExtendedFunctionEnable = 0x3A,

		// extended function bit on (RE=1)
		// display configured 8-bit data length (DL=1)
		// four-line or two-line display (N=1)
		// reverse display on (REV=1)
		ExtendedFunctionEnable_ReverseDisplay = 0x3B,

		// extended function bit off (RE=0)
		// IS=0
		// display configured 8-bit data length (DL=1)
		// four-line or two-line display (N=1)
		ExtendedFunctionDisable_InternalFunctionDisable = 0x38,

		// extended function bit off (RE=0)
		// IS=1
		// display configured 8-bit data length (DL=1)
		// four-line or two-line display (N=1)
		ExtendedFunctionDisable_InternalFunctionEnable = 0x39,

		// requires ExtendedFunctionEnable
		// use 6-dot font width (FW=1)
		// cursor black/white inversion off (B/W =0)
		// four-line or three-line display (NW=1)
		ExtendedFunction_FontSelection = 0x0D,

		// requires ExtendedFunctionEnable
		ExtendedFunction_UseFourLines = 0x09,

		// requires ExtendedFunctionEnable
		ExtendedFunction_BiDirection = 0x06,

		// requires ExtendedFunctionEnable
		ExtendedFunction_DotScroll = 0x1E,

		// requires ExtendedFunctionDisable_InternalFunctionDisable
		// or ExtendedFunctionDisable_InternalFunctionEnable
		ReturnHome = 0x02,

		// requires ExtendedFunctionEnable
		DisplayOn = 0x0C,     // no cursor

		// requires ExtendedFunctionEnable
		DisplayOn_CursorOn = 0x0E,   // cursor visible

		// requires ExtendedFunctionEnable
		DisplayOn_CursorOn_Blink = 0x0F,   // cursor blinking

		// requires ExtendedFunctionDisable_InternalFunctionEnable|Disable
		DisplayOff = 0x08,

		// requires ExtendedFunctionDisable_InternalFunctionEnable
		PowerBoosterOn = 0x54,

		// requires ExtendedFunctionDisable_InternalFunctionEnable
		PowerBoosterOff = 0x50,

		// requires ExtendedFunctionDisable_InternalFunctionEnable
		InternalFunction_Frequency_0 = 0x18,
		InternalFunction_Frequency_1 = 0x19,
		InternalFunction_Frequency_2 = 0x1A,
		InternalFunction_Frequency_3 = 0x1B,
		InternalFunction_Frequency_4 = 0x1C,
		InternalFunction_Frequency_5 = 0x1D,
		InternalFunction_Frequency_6 = 0x1E,
		InternalFunction_Frequency_7 = 0x1F,

		// requires ExtendedFunctionDisable_InternalFunctionEnable
		// divider on (Don=1)
		InternalFunction_Follower_AmpRatio_1 = 0x68,
		InternalFunction_Follower_AmpRatio_2 = 0x69,
		InternalFunction_Follower_AmpRatio_3 = 0x6A,
		InternalFunction_Follower_AmpRatio_4 = 0x6B,
		InternalFunction_Follower_AmpRatio_5 = 0x6C,
		InternalFunction_Follower_AmpRatio_6 = 0x6D,
		InternalFunction_Follower_AmpRatio_7 = 0x6E,
		InternalFunction_Follower_AmpRatio_8 = 0x6F,

		InternalFunction_BoosterOn_FollowerContrast_1 = 0x54,
		InternalFunction_BoosterOn_FollowerContrast_2 = 0x55,
		InternalFunction_BoosterOn_FollowerContrast_3 = 0x56,
		InternalFunction_BoosterOn_FollowerContrast_4 = 0x57,

		InternalFunction_Contrast_1 = 0x70,
		InternalFunction_Contrast_2 = 0x71,
		InternalFunction_Contrast_3 = 0x72,
		InternalFunction_Contrast_4 = 0x73,
		InternalFunction_Contrast_5 = 0x74,
		InternalFunction_Contrast_6 = 0x75,
		InternalFunction_Contrast_7 = 0x76,
		InternalFunction_Contrast_8 = 0x77,

		// requires ExtendedFunctionEnable
		ExtendedFunction_ROMSelection = 0x72, // (RS=1)
		// followed by
			ROMSelection_A = 0x00,
			ROMSelection_B = 0x04,
			ROMSelection_C = 0x05,

		ExtendedFunction_TemperatureCoefficient = 0x76,
		// followed by
			TemperatureCoefficient_05 = 0x02, // -0.05%
			TemperatureCoefficient_10 = 0x04, // -0.10%
			TemperatureCoefficient_15 = 0x06, // -0.15%
			TemperatureCoefficient_20 = 0x07, // -0.20%
	};

	enum Position : int
	{
		HOME = 0x80,
		LINE1 = 0x00,
		LINE2 = 0x20,
		LINE3 = 0x40,
		LINE4 = 0x60
	};

	struct Status
	{
		bool busy = true;
		uint8_t address_counter = 0;
		uint8_t part_id = 0;
	};

	DOGM204(I2CAddress address);
	virtual ~DOGM204();

	bool initialize();
	void write(const char* message);
	void clear();
	bool test();
	int numLines() { return 4; };
	int numColumns() { return 20; };

protected:
	const int _i2cAddress;

private:
	virtual bool _writeCommand(int command) = 0;
	virtual bool _writeCommands(int* commands, int length) = 0;
	virtual bool _writeData(const uint8_t * bytes, size_t length) = 0;
	virtual bool fetchStatus(Status& status) = 0;
	virtual Status _waitUntilReady() = 0;
	virtual void _reset() = 0;
};

} // namespace robofish::edc
