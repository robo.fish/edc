#pragma once
#include "stm32l4xx_hal.h"

/// @return 0 if initialized successfully
#ifdef __cplusplus
extern "C" {
#endif

void app(
	I2C_HandleTypeDef* i2c,
	UART_HandleTypeDef* uart,
	IWDG_HandleTypeDef* iwdg,
	GPIO_TypeDef* led_pin_port,
	uint16_t led_pin
);

#ifdef __cplusplus
}
#endif
