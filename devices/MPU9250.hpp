#pragma once
#include "../utils/Initializer.hpp"

namespace robofish::edc {

struct MPU9250 : public Initializable, Testable
{
	struct Values
	{
		/// temperature in degrees Celsius
		float temperature = 0.f;

		/// angular rate (degrees per second) of movement around the X axis
		int gyroX = 0;
	};

	// the 8-bit I2C address
	enum I2CAddress
	{
		AD0_Low = 0xD0,
		AD0_High = 0xD2
	};

	enum Register : int
	{
		XG_OFFSET_H   = 0x13,
		XG_OFFSET_L   = 0x14,
		SMPLRT_DIV    = 0x19,
		CONFIG        = 0x1A,
		GYRO_CONFIG   = 0x1B,
		ACCEL_CONFIG  = 0x1C,
		ACCEL_CONFIG2 = 0x1D,
		WOM_THR       = 0x1F, // threshold for wake on motion
		FIFO_EN       = 0x23,
		INT_PIN_CFG   = 0x37,
		INT_ENABLE    = 0x38,
		INT_STATUS    = 0x3A,
		ACCEL_XOUT_H  = 0x3B,
		ACCEL_XOUT_L  = 0x3C,
		ACCEL_YOUT_H  = 0x3D,
		ACCEL_YOUT_L  = 0x3E,
		ACCEL_ZOUT_H  = 0x3F,
		ACCEL_ZOUT_L  = 0x40,
		TEMP_OUT_H    = 0x41,
		TEMP_OUT_L    = 0x42,
		GYRO_XOUT_H   = 0x43,
		GYRO_XOUT_L   = 0x44,
		GYRO_YOUT_H   = 0x45,
		GYRO_YOUT_L   = 0x46,
		GYRO_ZOUT_H   = 0x47,
		GYRO_ZOUT_L   = 0x48,
		PWR_MGMT_1    = 0x6B,
		PWR_MGMT_2    = 0x6C,
		FIFO_COUNTH   = 0x72,
		FIFO_COUNTL   = 0x73,
		FIFO_R_W      = 0x74,
		WHOAMI        = 0x75,
	};

	struct GyroConfig
	{
		enum Flag : int
		{
			SelfTestX = 0x80,
			SelfTestY = 0x40,
			SelfTestZ = 0x20,
			ScaleSelect_250 = 0x00,
			ScaleSelect_500 = 0x08,
			ScaleSelect_1000 = 0x10,
			ScaleSelect_2000 = 0x18
		};

		/// @return register content value change rate per degrees/second (dps) movement of the device
		constexpr static float sensitivity(Flag flag)
		{
			switch (flag)
			{
				case Flag::ScaleSelect_250: return 131.0f;
				case Flag::ScaleSelect_500: return 65.5f;
				case Flag::ScaleSelect_1000: return 32.8f;
				case Flag::ScaleSelect_2000: return 16.4f;
				default: return 1.f;
			}
		}
	};

	enum InterruptConfig : int
	{
		BYPASS_EN         = 0x02,  // whether the I2C pins should be bypassed (1) or not (0) by pulling up to VDD.
		FSYNC_INT_MODE_EN = 0x04,  // whether the FSYNC pin is used as an interrupt (1) or not (0)
		ACTL_FSYNC        = 0x08,  // whether the logic level for the FSYNC pin as an interrupt is active low (1) or active high (0)
		INT_ANYRD_2CLEAR  = 0x10,  // whether the interrupt status is cleared if any read operation is performed (1) or only by reading INT_STATUS
		LATCH_INT_EN      = 0x20,  // whether the INT pin level is held until interrupt status is cleared (1) or is a pulse with width 50 microsec
		OPEN              = 0x40,  // whether the INT pin is configured as open drain (1) or push-pull (0)
		ACTL              = 0x80   // whether the logic level for INT pin is active low (1) or active high (0)
	};

	enum InterruptStatus : int
	{
		RAW_DATA_RDY_INT = 0x01,
		FSYNC_INT        = 0x08,
		FIFO_OFLOW_INT   = 0x10,
		WOM_INT          = 0x40
	};

	enum FIFOConfig : int
	{
		ACCEL        = 0x08,
		GYRO_ZOUT    = 0x10,
		GYRO_YOUT    = 0x20,
		GYRO_XOUT    = 0x40,
		TEMP_FIFO_EN = 0x80
	};

	enum class WhoAmI : int
	{
		MPU6500 = 0x70, // = MPU9250 without magnetometer
		MPU9250 = 0x71,
		MPU9255 = 0x73,
		MPU6515 = 0x75
	};

	enum PowerManagement1 : int
	{
		H_RESET = 0x80, // flag will auto-clear after setting
		SLEEP = 0x40,   // set to sleep mode
		CYCLE = 0x20,   // cycles between sleep and sampling at a rate LP_ACCEL_ODR
		GYRO_STANDBY = 0x10,
		PD_TAT = 0x08
	};

	enum PowerManagement2 : int
	{
		DISABLE_XA = 0x20, // disable accelerometer X axis
		DISABLE_YA = 0x10, // disable accelerometer Y axis
		DISABLE_ZA = 0x80, // disable accelerometer Z axis
		DISABLE_XG = 0x04, // disable gyro X axis
		DISABLE_YG = 0x02, // disable gyro Y axis
		DISABLE_ZG = 0x01, // disable gyro Z axis
	};

	MPU9250(I2CAddress address);
	virtual ~MPU9250();

	bool test();
	bool initialize();
	void readValues(MPU9250::Values & values);
	void onInterrupt();

protected:
	const int _i2cAddress;

private:
	GyroConfig::Flag gyroScale;
	float gyroSensitivity;
	int TEMP_OUT = 0; // signed 16-bit value
	int GYRO_OUT = 0; // signed 16-bit value

	void _updateValues();

	/**
	 * To write the internal MPU-9250 registers, the master transmits the start condition (S),
	 * followed by the I2C address and the write bit (0). At the 9th clock cycle (when the
	 * clock is high), the MPU-9250 acknowledges the transfer. Then the master puts the
	 * register address (RA) on the bus. After the MPU-9250 acknowledges the reception of
	 * the register address, the master puts the register data onto the bus. This is followed
	 * by the ACK signal, and data transfer may be concluded by the stop condition (P).
	 * To write multiple bytes after the last ACK signal, the master can continue outputting
	 * data rather than transmitting a stop signal. In this case, the MPU-9250 automatically
	 * increments the register address and loads the data to the appropriate register.
	 * 
	 * Single-Byte Write Sequence
	 *          ______________________________________________
	 *   Master | S | AD+W |     | RA |     | DATA |     | P |
	 *          ----------------------------------------------
	 *   Slave  |   |      | ACK |    | ACK |      | ACK |   |
	 *          ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
	 * Burst Write Sequence
	 *          ___________________________________________________________
	 *   Master | S | AD+W |     | RA |     | DATA |     | DATA |     | P |
	 *          -----------------------------------------------------------
	 *   Slave  |   |      | ACK |    | ACK |      | ACK |      | ACK |   |
	 *          ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
	 */
	virtual bool _writeSingleByte(MPU9250::Register targetRegister, int value) = 0;

	/**
	 * Double-Byte Read Sequence
	 *          _____________________________________________________________________________
	 *   Master | S | AD+W |     | RA |     | S | AD+R |     |      | ACK |      | NACK | P |
	 *          -----------------------------------------------------------------------------
	 *   Slave  |   |      | ACK |    | ACK |          | ACK | DATA |     | DATA |      |   |
	 *          ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
	 */
	virtual bool _readDoubleByte(MPU9250::Register startRegister, int& value) = 0;

	/** 
	 * @return an integer where only the 8 lowest bits are relevant
	 * 
	 * To read the internal MPU-9250 registers, the master sends a start condition, followed by
	 * the I2C address and a write bit, and then the register address (RA) that is going to be
	 * read. Upon receiving the ACK signal from the MPU-9250, the master transmits a start signal
	 * followed by the slave address and read bit. As a result, the MPU-9250 sends an ACK signal
	 * and the data. The communication ends with a not acknowledge (NACK) signal from master and
	 * a stop bit from master. The NACK condition is defined such that the SDA line remains high
	 * at the 9th clock cycle. The following figures show single and two-byte read sequences.
	 *
	 * Single-Byte Read Sequence
	 *          ________________________________________________________________
	 *   Master | S | AD+W |     | RA |     | S | AD+R |     |      | NACK | P |
	 *          ----------------------------------------------------------------
	 *   Slave  |   |      | ACK |    | ACK |          | ACK | DATA |      |   |
	 *          ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
	 */
	virtual bool _readSingleByte(MPU9250::Register targetRegister, int& value) = 0;

	virtual void _disableIRQ() = 0;
	virtual void _enableIRQ() = 0;
	virtual void _wait(int milliseconds) = 0;
};

} // namespace robofish::edc
