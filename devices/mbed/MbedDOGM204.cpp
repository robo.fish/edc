#include "MbedDOGM204.hpp"

#ifdef __MBED__

#include <mbed.h>
#include <rtos.h>
#include <hal/i2c_api.h>
#include <chrono>
#include "../../utils/mbed/MbedLogger.hpp"

using namespace robofish::edc;
using namespace std::chrono_literals;

MbedDOGM204::MbedDOGM204(mbed::DigitalOut& resetPin, mbed::I2C& i2c, DOGM204::I2CAddress i2cAddress) :
	DOGM204(i2cAddress),
	_i2c(i2c),
	_resetPin(resetPin)
{
}

MbedDOGM204::~MbedDOGM204() {}

bool MbedDOGM204::_writeCommand(int command)
{
	//while (isBusy()) ;
	char data[2] = { ControlByte::OneCommand, (char)command };
	return _i2c.write(_i2cAddress, data, sizeof(data)) == 0;
}

bool MbedDOGM204::_writeCommands(int* commands, int length)
{
	volatile bool success = true;
	char controlAndCommand[2] = { ControlByte::OneCommand, 0 };

	for (int i = 0; i < length; ++i)
	{
		controlAndCommand[1] = commands[i];
	#if 1
		success = success && (_i2c.write(_i2cAddress, controlAndCommand, sizeof(controlAndCommand), i < (length - 1)) == 0);
	#else
		_i2c.write(_i2cAddress, controlAndCommand, sizeof(controlAndCommand), i < (length - 1));
	#endif
	}
	return success;
}

bool MbedDOGM204::_writeData(const uint8_t * bytes, size_t length)
{
	char buffer[82];
	_waitUntilReady();
	const size_t payloadSize = length < sizeof(buffer) ? length : sizeof(buffer) - 1;
	buffer[0] = ControlByte::AllData;
	memcpy(buffer + 1, bytes, payloadSize);
	return _i2c.write(_i2cAddress, buffer, payloadSize + 1) == 0;
}

bool MbedDOGM204::fetchStatus(Status& status)
{
	char control = ControlByte::OneCommand;
	char busyAndPartID[2] = { 0, 0 };
	volatile int response = _i2c.write(_i2cAddress, &control, 1);
	response = _i2c.read(_i2cAddress, busyAndPartID, sizeof(busyAndPartID));
	if (response == 0)
	{
		status.busy = (busyAndPartID[0] & 0x80) != 0;
		status.address_counter = busyAndPartID[0] & 0x7F;
		status.part_id = busyAndPartID[1] & 0x7F;
		return true;
	}
	return false;
}

void MbedDOGM204::_reset()
{
	if (_resetPin.read() == 0)
	{
		_resetPin.write(1);
	}
	wait_us(200); // microseconds
	_resetPin.write(0);
	wait_us(5);
	_resetPin.write(1);
	wait_us(200);
}

DOGM204::Status MbedDOGM204::_waitUntilReady()
{
	DOGM204::Status status;
	fetchStatus(status);
	while (!fetchStatus(status) || status.busy)
	{
		rtos::ThisThread::sleep_for(5ms);
	}
	return status;
}

#endif // __MBED__
