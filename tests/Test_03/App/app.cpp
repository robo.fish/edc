#include "app.h"
#include "devices/stm32/STM32_24FC64.hpp"
#include "devices/stm32/STM32_MPU9250.hpp"
#include "utils/stm32/Watchdog.hpp"
#include "utils/stm32/STM32_Logger.hpp"
#include "utils/Initializer.hpp"
#include <functional>

using namespace robofish::edc;

#define APP_USES_EEPROM (1)
#define APP_USES_MPU (1)

std::function<void (void)> gpio_8_interrupt_handler;

extern "C" void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if ((GPIO_Pin == GPIO_PIN_8) && gpio_8_interrupt_handler)
	{
		gpio_8_interrupt_handler();
	}
}

void app(
	I2C_HandleTypeDef* i2c,
	UART_HandleTypeDef* uart,
	IWDG_HandleTypeDef* iwdg,
	GPIO_TypeDef* led_pin_port,
	uint16_t led_pin
)
{
	STM32_Logger::initialize(uart);
	HAL_Delay(50);
	STM32_Logger logger;

#if APP_USES_EEPROM
	STM32_24FC64 eeprom(i2c);
	Initializer(logger).testedInitialization(eeprom, "24FC64 EEPROM");
#endif

#if APP_USES_MPU
	STM32_MPU9250 mpu(i2c);
	gpio_8_interrupt_handler = [&mpu](void) { mpu.onInterrupt(); };
	Initializer(logger).testedInitialization(mpu, "MPU-9250");
	MPU9250::Values mpuValues;
#endif

	Watchdog::initialize(iwdg);
	int loopCounter = 0;
	while (true)
	{
		HAL_Delay(500);
		HAL_GPIO_TogglePin(led_pin_port, led_pin);

	#if APP_USES_MPU
		if (loopCounter % 5 == 0)
		{
			mpu.readValues(mpuValues);
			logger.write("MPU gyro = %d  , temperature = %.1f\n\r", mpuValues.gyroX, mpuValues.temperature);
		}
	#endif

		loopCounter++;
		Watchdog::reset();
	}
}
