#pragma once

#ifdef __MBED__

#include <memory>
#include <mbed.h>
#include "../DOGM204.hpp"

namespace robofish::edc {

struct MbedDOGM204 final : public DOGM204
{
	MbedDOGM204(mbed::DigitalOut& resetPin, mbed::I2C& i2c, DOGM204::I2CAddress i2cAddress = I2CAddress::SA0_Low);
	~MbedDOGM204();

private:
	bool _writeCommand(int command);
	bool _writeCommands(int* commands, int length);
	bool _writeData(const uint8_t * bytes, size_t length);
	bool fetchStatus(Status& status);
	Status _waitUntilReady();
	void _reset();

	mbed::I2C& _i2c;
	mbed::DigitalOut& _resetPin;
};

} // namespace robofish::edc

#endif // __MBED__
