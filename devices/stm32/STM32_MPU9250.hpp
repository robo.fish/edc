#pragma once
#include "../MPU9250.hpp"

#if !defined(__MBED__)

#include <memory>
#include "stm32l4xx_hal.h"

namespace robofish::edc {

struct STM32_MPU9250 final : public MPU9250
{
	STM32_MPU9250(I2C_HandleTypeDef* i2c, MPU9250::I2CAddress address = MPU9250::I2CAddress::AD0_Low);
	~STM32_MPU9250();

private:
	bool _writeSingleByte(MPU9250::Register targetRegister, int value);
	bool _readSingleByte(MPU9250::Register targetRegister, int& value);
	bool _readDoubleByte(MPU9250::Register startRegister, int& value);
	void _enableIRQ();
	void _disableIRQ();
	void _wait(int milliseconds);

	I2C_HandleTypeDef* const _i2c;
};

} // namespace robofish::edc

#endif // !defined(__MBED__)
