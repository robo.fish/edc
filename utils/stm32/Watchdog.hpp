#pragma once

#if !defined(__MBED__)

#include "stm32l4xx_hal.h"

/**
 * Make sure the STM32 independent watchdog (IWDG) is configured.
 */
struct Watchdog
{
	/**
	 * @param duration in milliseconds
	 */
	static bool initialize(int duration);

	static bool initialize(IWDG_HandleTypeDef* iwdgHandle);

	static void reset();
};

#endif
