#include <mbed.h>
#include <stdarg.h>
#include <PinNames.h>
#include "utils/mbed/MbedLogger.hpp"
#include "utils/Initializer.hpp"

using namespace robofish::edc;

#define USING_STORAGE (1)
#define USING_MPU (1)

#if defined(TARGET_NUCLEO_F411RE) || defined(TARGET_NUCLEO_L432KC)
#define mpuInterruptPinName PA_8
#elif
#pragma GCC error "The MPU interrupt pin is not specified."
#endif

DigitalOut led1(LED1);
mbed::I2C i2c(I2C_SDA, I2C_SCL);

#if USING_STORAGE
#include "devices/mbed/Mbed24FC64.hpp"
Mbed24FC64 storage(i2c);
#endif

#if USING_MPU
#include "devices/mbed/MbedMPU9250.hpp"
mbed::InterruptIn mpuInterruptPin(mpuInterruptPinName);
MbedMPU9250 mpu(mpuInterruptPin, i2c);
MPU9250::Values mpuValues;
#endif

void setup();
void loop();
static int32_t loopCounter = 0;

int main()
{
	MbedLogger::initialize();
	MbedLogger logger;
	logger.printSystemInfo();

	i2c.frequency(400000);

#if USING_STORAGE
	Initializer(logger).testedInitialization(storage, "24FC64 EEPROM");
#endif

#if USING_MPU
	Initializer(logger).testedInitialization(mpu, "MPU-9250");
#endif

	while (true)
	{
	if (loopCounter % 5 == 0)
	{
		led1 = !led1;
	}

#if USING_MPU
	if (loopCounter % 20 == 0)
	{
		mpu.readValues(mpuValues);
		logger.write("MPU gyro = %d  , temperature = %.1f\n", mpuValues.gyroX, mpuValues.temperature);
	}
#endif

		++loopCounter;
		rtos::ThisThread::sleep_for(100ms);
	}
}
