#include "MbedLogger.hpp"

#ifdef __MBED__

#include "stdio.h"
#include "mbed.h"

using namespace robofish::edc;

static BufferedSerial serial(USBTX, USBRX);

void MbedLogger::initialize(uint32_t baud_rate)
{
	serial.set_baud(baud_rate);
	// Giving the serial connection some time to initialize
	rtos::ThisThread::sleep_for(50ms);
}

void MbedLogger::write(const char * message, ...)
{
	va_list args;
	va_start(args, message);
	char buffer[250];
	vsnprintf(buffer, 250, message, args);
	serial.write(buffer, strlen(buffer));
}

void MbedLogger::printSystemInfo()
{
	const reset_reason_t reason = ResetReason::get();
	const char* divider = "======================================";
	const char* resetMessage = "";
	switch (reason)
	{
		case RESET_REASON_POWER_ON:
				resetMessage = "Power On";
				break;
		case RESET_REASON_PIN_RESET:
				resetMessage = "Reset by hardware pin";
				break;
		case RESET_REASON_SOFTWARE:
				resetMessage = "Software Reset";
				break;
		case RESET_REASON_WATCHDOG:
				resetMessage = "Reset by watchdog";
				break;
		default:
				resetMessage = "Reset";
	}
	write("%s\n%s\n%s\n", divider, resetMessage, divider);
	write("Mbed OS %d.%d.%d (RTOS: %s)\n%s\n",
		MBED_MAJOR_VERSION, MBED_MINOR_VERSION, MBED_PATCH_VERSION,
	#ifdef MBED_CONF_RTOS_PRESENT
		"yes",
	#else
		"no",
	#endif
		divider
	);
}

#endif // MBED
