#include "STM32_Logger.hpp"

#if !defined(__MBED__)

#include <cstdio>
#include <cstring>
#include <cstdarg>

using namespace robofish::edc;

HAL_StatusTypeDef a;
static UART_HandleTypeDef* _uart = nullptr;

void STM32_Logger::initialize(UART_HandleTypeDef* uart)
{
	_uart = uart;
}

bool STM32_Logger::initialize(uint32_t baud_rate)
{
	auto uart = new UART_HandleTypeDef;
	uart->Instance = USART2;
	uart->Init.BaudRate = baud_rate;
	uart->Init.WordLength = UART_WORDLENGTH_8B;
	uart->Init.StopBits = UART_STOPBITS_1;
	uart->Init.Parity = UART_PARITY_NONE;
	uart->Init.Mode = UART_MODE_TX_RX;
	uart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	uart->Init.OverSampling = UART_OVERSAMPLING_16;
	uart->Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	uart->AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(uart) != HAL_OK)
	{
		delete uart;
		return false;
	}
	_uart = uart;
	return false;
}

void STM32_Logger::write(const char * message, ...)
{
	char buffer[250];
	if (_uart != nullptr)
	{
		va_list args;
		va_start(args, message);
		vsnprintf(buffer, 250, message, args);
		HAL_UART_Transmit(_uart, (uint8_t*)buffer, strlen(buffer), 100);
	}
}

void STM32_Logger::printSystemInfo()
{
}

#endif // !defined(__MBED__)
