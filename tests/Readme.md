# EDC test applications

## Test_01:
* STMicroelectronics NUCLEO-F411RE, NUCLEO-L432KC
* DOGM204-A LCD display
* Mbed
* I2C
* PlatformIO, Mbed Studio

## Test_02
* STMicroelectronics NUCLEO-F411RE, NUCLEO-L432KC
* 24FC64 EEPROM
* MPU-92/65 (Invensense MPU-9250)
* Mbed
* I2C
* PlatformIO, Mbed Studio

## Test_03
* STMicroelectronics NUCLEO-L432KC
* 24FC64 EEPROM
* MPU-92/65 (Invensense MPU-9250)
* STM32
* I2C
* STM32CubeIDE

