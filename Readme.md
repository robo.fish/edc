# Electronic Device Control (EDC) library

EDC is a source code library for controlling various electronic devices
(sensors, motors, displays etc.) in Arm Cortex-M microcontroller applications.

The folder [devices](devices) contains the sources for the devices.

The [tests](tests) folder contains test applications that use the device library.
If a test application folder contains a 'platform.ini' file, you will need
[Visual Studio Code](code.visualstudio.com) and [PlatformIO](platformio.org)
to build and run the application. To open a PlatformIO project in Visual Studio
Code, choose the folder of the test application as the root project folder.

The [kicad](kicad) folder contains a component library for [KiCad](kicad.org).
KiCad is used to create the circuit design files found in the test applications.

