#pragma once

#include "Logger.hpp"
#include <type_traits>

namespace robofish::edc {

struct Initializable
{
	virtual bool initialize() = 0;
};

struct Testable
{
	virtual bool test() = 0;
};


struct Initializer
{
	Logger& logger;

	Initializer(Logger& logger_) : logger(logger_)
	{}

	template<typename T> bool testedInitialization(T testable, const char* label)
	{
		bool success = false;
		if ( std::is_base_of<Initializable, T>::value )
		{
			logger.write("Initializing %s display...", label);
			success = testable.initialize();
			logger.write("%s.", success ? "done" : "failed");
			if ( std::is_base_of<Testable, T>::value && success )
			{
				success = testable.test();
				logger.write(" Test %s.", success ? "passed" : "failed");
			}
			logger.write("\n\r");
		}
		return success;
	}
};

} // namespace robofish::edc
