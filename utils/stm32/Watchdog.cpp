#include "Watchdog.hpp"

#if !defined(__MBED__)

#include "stm32l4xx_hal.h"

IWDG_HandleTypeDef* _iwdg;

bool Watchdog::initialize(IWDG_HandleTypeDef* iwdgHandle)
{
	_iwdg = iwdgHandle;
	return true;
}

bool Watchdog::initialize(int duration)
{
	auto iwdg = new IWDG_HandleTypeDef;
	iwdg->Instance = IWDG;
	iwdg->Init.Prescaler = IWDG_PRESCALER_256;
	iwdg->Init.Window = 4095;
	iwdg->Init.Reload = 4095;
	if (HAL_IWDG_Init(iwdg) != HAL_OK)
	{
		delete iwdg;
		return false;
	}
	_iwdg = iwdg;
	return true;
}

void Watchdog::reset()
{
	if (_iwdg != nullptr)
	{
		HAL_IWDG_Refresh(_iwdg);
	}
}

#endif
