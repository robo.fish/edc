#pragma once

#include "../24FC64.hpp"

#if __MBED__

#include <mbed.h>
#include <memory>

namespace robofish::edc {

struct Mbed24FC64 final : public EEPROM_24FC64
{
	Mbed24FC64(mbed::I2C& ic2, EEPROM_24FC64::I2CAddress address = EEPROM_24FC64::I2CAddress::Address_000);
	~Mbed24FC64();

private:
	mbed::I2C& _i2c;

	bool _writeByte(uint16_t address, uint8_t byte);
	bool _writePage(uint16_t startAddress, size_t length, const uint8_t * bytes);
	bool _readRandomByte(uint16_t address, uint8_t* out);
	bool _readSequentialBytes(uint16_t startAddress, size_t length, uint8_t* outputBuffer);
	void _waitForWriteCompletion();
};

} // namespace robofish::edc

#endif // __MBED__
