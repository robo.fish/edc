#include "DOGM204.hpp"
#include <cstring>

using namespace robofish::edc;

DOGM204::DOGM204(I2CAddress address) :
	_i2cAddress((int)address)
{}

DOGM204::~DOGM204()
{}

void DOGM204::write(const char* message)
{
	_waitUntilReady();
	_writeData((const uint8_t *)message, strlen(message));
}

void DOGM204::clear()
{
	int commands[] = { ClearDisplay, ReturnHome };
	_writeCommands(commands, sizeof(commands)/sizeof(int));
}

bool DOGM204::test()
{
	DOGM204::Status status;
	return fetchStatus(status) && (status.part_id == 0x1A);
}

bool DOGM204::initialize()
{
	_reset();

	// Initialization command sequence.
	// The first command after the reset must be ExtendedFunctionEnabel (0x3A)
	int commands[] = {
		ExtendedFunctionEnable,
		ExtendedFunction_UseFourLines,
		ExtendedFunction_BiDirection,
		ExtendedFunction_DotScroll,
		ExtendedFunctionDisable_InternalFunctionEnable,
		InternalFunction_Frequency_3,
		InternalFunction_Follower_AmpRatio_7,
		InternalFunction_BoosterOn_FollowerContrast_4,
		InternalFunction_Contrast_8,
		ExtendedFunctionDisable_InternalFunctionDisable,
		ClearDisplay,
		ReturnHome,
		DisplayOn_CursorOn_Blink
	};

	return _writeCommands(commands, sizeof(commands)/sizeof(int));
}
