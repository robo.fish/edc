#include "24FC64.hpp"

using namespace robofish::edc;

EEPROM_24FC64::EEPROM_24FC64(I2CAddress address) :
	_i2cAddress((int)address)
{
}

EEPROM_24FC64::~EEPROM_24FC64()
{
}

bool EEPROM_24FC64::initialize()
{
	return true;
}

bool EEPROM_24FC64::test()
{
	bool passed = false;
	uint8_t test = 0x46;
	uint8_t retrieved = 0x00;
	write(0, 1, &test);
	read(0, 1, &retrieved);
	passed = (test == retrieved);
	return passed;
}

bool EEPROM_24FC64::write(uint16_t startAddress, size_t length, const uint8_t * bytes)
{
	if (length == 0) { return true; }

	// determine the number of page writes needed
	// for each page write,
	//   determine the start address
	//   determine the byte offset and length
	//   call writePage()

	return _writeByte(startAddress, *bytes);
}


bool EEPROM_24FC64::read(uint16_t startAddress, size_t length, uint8_t* outputBuffer)
{
	return _readRandomByte(startAddress, outputBuffer);
}
