#include "STM32_MPU9250.hpp"

#if !defined(__MBED__)

using namespace robofish::edc;

STM32_MPU9250::STM32_MPU9250(I2C_HandleTypeDef* i2c, MPU9250::I2CAddress i2cAddress) :
	MPU9250(i2cAddress),
	_i2c(i2c)
{}

STM32_MPU9250::~STM32_MPU9250()
{}

bool STM32_MPU9250::_writeSingleByte(Register targetRegister, int value)
{
	return HAL_I2C_Mem_Write(_i2c, _i2cAddress, targetRegister, I2C_MEMADD_SIZE_8BIT, (uint8_t*)&value, 1, HAL_MAX_DELAY) == HAL_OK;
}

bool STM32_MPU9250::_readSingleByte(Register targetRegister, int& value)
{
	uint8_t result = 0;
	const bool success = (HAL_I2C_Mem_Read(_i2c, _i2cAddress, targetRegister, I2C_MEMADD_SIZE_8BIT, &result, 1, HAL_MAX_DELAY) == HAL_OK);
	value = (int)result;
	return success;
}

bool STM32_MPU9250::_readDoubleByte(Register startRegister, int& value)
{
	uint16_t result = 0;
	const bool success = (HAL_I2C_Mem_Read(_i2c, _i2cAddress, startRegister, I2C_MEMADD_SIZE_8BIT, (uint8_t*)&result, 2, HAL_MAX_DELAY) == HAL_OK);
	value = (int)result;
	return success;
}

void STM32_MPU9250::_enableIRQ()
{
	__enable_irq();
}

void STM32_MPU9250::_disableIRQ()
{
	__disable_irq();
}

void STM32_MPU9250::_wait(int millis)
{
	HAL_Delay(millis);
}

#endif // !defined(__MBED__)
