#pragma once

#include "../24FC64.hpp"

#if !defined(__MBED__)

#include <memory>
#include "stm32l4xx_hal.h"

namespace robofish::edc {

struct STM32_24FC64 final : public EEPROM_24FC64
{
	STM32_24FC64(I2C_HandleTypeDef* i2c, EEPROM_24FC64::I2CAddress address = EEPROM_24FC64::I2CAddress::Address_000);
	~STM32_24FC64();

private:
	I2C_HandleTypeDef* const _i2c;

	bool _writeByte(uint16_t address, uint8_t byte);
	bool _writePage(uint16_t startAddress, size_t length, const uint8_t * bytes);
	bool _readRandomByte(uint16_t address, uint8_t* out);
	bool _readSequentialBytes(uint16_t startAddress, size_t length, uint8_t* outputBuffer);
	void _waitForWriteCompletion();
};

} // namespace robofish::edc

#endif // !defined(__MBED__)
