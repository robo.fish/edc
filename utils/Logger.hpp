#pragma once

namespace robofish::edc {

struct Logger
{
	virtual void write(const char * message, ...) = 0;

	virtual void printSystemInfo() = 0;
};

} // namespace robofish::edc
