#pragma once
#include <cstdint>
#include <cstdlib>
#include "../utils/Initializer.hpp"

namespace robofish::edc {

struct EEPROM_24FC64 : public Initializable, Testable
{
	enum I2CAddress : uint8_t
	{
		Address_000 = 0xA0,
		Address_001 = 0xA1,
		Address_010 = 0xA2,
		Address_011 = 0xA3,
		Address_100 = 0xA4,
		Address_101 = 0xA5,
		Address_110 = 0xA6,
		Address_111 = 0xA7
	};

	static const int kPageSize = 32;

	EEPROM_24FC64(I2CAddress address);
	virtual ~EEPROM_24FC64();

	bool initialize();
	bool test();
	bool write(uint16_t startAddress, size_t length, const uint8_t * bytes);
	bool read(uint16_t startAddress, size_t length, uint8_t * buffer);

protected:
	const int _i2cAddress;

private:
	/**
	 *  Master  | S | Control |     | AddrHigh |     | AddrLow |     | Data |     | P |
	 *          -----------------------------------------------------------------------
	 *  Slave   |   |         | ACK |          | ACK |         | ACK |      | ACK |   |
	 *
	 *  where Control is the byte |1|0|1|0|A2|A1|A0|0|
	 *  AddrHigh is the address high byte where the most significant three bits don't matter.
	 */
	virtual bool _writeByte(uint16_t address, uint8_t byte) = 0;

  /**
	 *  Master  | S | Control |     | AddrHigh |     | AddrLow |     | Data Byte 0 |     |     | Data Byte N |     | P |
	 *          -------------------------------------------------------------------------- ... --------------------------
	 *  Slave   |   |         | ACK |          | ACK |         | ACK |             | ACK |                    | ACK |
	 *
	 *  The number of data bytes should be chosen to fit inside the page ( Address / kPageSize ), starting from Address.
	 *  If the data exceeds the remaining page capacity, writing will wrap around to the start of the page.
	 *  The reason for this behavior, is that the EEPROM temporarily stores the received bytes in a page-sized buffer
	 *  before committing to memory.
	 */
	virtual bool _writePage(uint16_t startAddress, size_t length, const uint8_t * bytes) = 0;

	virtual bool _readRandomByte(uint16_t address, uint8_t* out) = 0;
	virtual bool _readSequentialBytes(uint16_t startAddress, size_t length, uint8_t* outputBuffer) = 0;

	virtual void _waitForWriteCompletion() = 0;

};

} // namespace robofish::edc
