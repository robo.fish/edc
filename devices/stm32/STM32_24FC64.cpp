#include "STM32_24FC64.hpp"

#if !defined(__MBED__)

#include "../../utils/stm32/STM32_Logger.hpp"
#include "stm32l4xx_hal.h"

using namespace robofish::edc;

STM32_24FC64::STM32_24FC64(I2C_HandleTypeDef* i2c, EEPROM_24FC64::I2CAddress address) :
	EEPROM_24FC64(address),
	_i2c(i2c)
{}

STM32_24FC64::~STM32_24FC64()
{}

bool STM32_24FC64::_writeByte(uint16_t address, uint8_t byte)
{
	bool success = (HAL_I2C_Mem_Write(_i2c, _i2cAddress, address, I2C_MEMADD_SIZE_16BIT, &byte, 1, HAL_MAX_DELAY) == HAL_OK);
	_waitForWriteCompletion();
	return success;
}

bool STM32_24FC64::_writePage(uint16_t startAddress, size_t length, const uint8_t * bytes)
{
	if (length == 0) { return true; }

	//   generate start condition
	//   write up to kPageSize
//		bool success = (_i2c.write(startAddress >> 8) == 1);
//		success = success && (_i2c.write(startAddress) == 1);
//		_i2c.stop();
//		_waitForWriteCompletion();
//		return success;
	return false;
}

bool STM32_24FC64::_readRandomByte(uint16_t address, uint8_t* out)
{
	return (HAL_I2C_Mem_Read(_i2c, _i2cAddress, address, I2C_MEMADD_SIZE_16BIT, out, 1, HAL_MAX_DELAY) == HAL_OK);
}

bool STM32_24FC64::_readSequentialBytes(uint16_t startAddress, size_t length, uint8_t* outputBuffer)
{
	return false;
}

void STM32_24FC64::_waitForWriteCompletion()
{
	int acknowledged = HAL_BUSY;
	uint8_t empty = 0;
	while (acknowledged != HAL_OK)
	{
		acknowledged = HAL_I2C_Master_Transmit(_i2c, _i2cAddress, &empty, 0, HAL_MAX_DELAY);
	}
}

#endif // !defined(__MBED__)
