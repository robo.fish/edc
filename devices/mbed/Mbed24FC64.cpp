#include "Mbed24FC64.hpp"

#ifdef __MBED__

using namespace robofish::edc;

Mbed24FC64::Mbed24FC64(mbed::I2C & i2c, EEPROM_24FC64::I2CAddress address) :
	EEPROM_24FC64(address),
	_i2c(i2c)
{

}

Mbed24FC64::~Mbed24FC64()
{

}

bool Mbed24FC64::_writeByte(uint16_t address, uint8_t byte)
{
	char data[3];
	data[0] = ((char*)(&address))[0];
	data[1] = ((char*)(&address))[1];
	data[2] = byte;
	bool success = _i2c.write(_i2cAddress, data, sizeof(data)) == 0;
	_waitForWriteCompletion();
	return success;
}

bool Mbed24FC64::_writePage(uint16_t startAddress, size_t length, const uint8_t * bytes)
{
	if (length == 0) { return true; }

	//   generate start condition
	//   write up to kPageSize
	bool success = (_i2c.write(startAddress >> 8) == 1);
	success = success && (_i2c.write(startAddress) == 1);
	_i2c.stop();
	_waitForWriteCompletion();
	return success;
}

bool Mbed24FC64::_readRandomByte(uint16_t address, uint8_t* out)
{
	_i2c.write(_i2cAddress, (const char *)&address, 2, true);
	return _i2c.read(_i2cAddress, (char*)out, 1) == 0;
}

bool Mbed24FC64::_readSequentialBytes(uint16_t startAddress, size_t length, uint8_t* outputBuffer)
{
	return false;
}

void Mbed24FC64::_waitForWriteCompletion()
{
	int acknowledged = 1;
	while (acknowledged != 0)
	{
		acknowledged = _i2c.write(_i2cAddress, 0, 0, true);
	}
}

#endif // __MBED__
