#pragma once
#include "../Logger.hpp"

#if !defined(__MBED__)

#include "stm32l4xx_hal.h"

namespace robofish::edc {

struct STM32_Logger final : public Logger
{
	static bool initialize(uint32_t baud_rate = 115200);

	/// Creates a Logger from a readily initialized UART instance.
	static void initialize(UART_HandleTypeDef* uart);

	void write(const char * message, ...);

	void printSystemInfo();
};

} // namespace robofish::edc

#endif
