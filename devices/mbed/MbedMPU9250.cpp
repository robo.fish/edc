#include "MbedMPU9250.hpp"

#ifdef __MBED__

#include "mbed.h"
#include "PinNames.h"

using namespace robofish::edc;

MbedMPU9250::MbedMPU9250(mbed::InterruptIn& interruptPin, mbed::I2C& i2c, MPU9250::I2CAddress i2cAddress) :
	MPU9250(i2cAddress),
	_i2c(i2c),
	_irq(interruptPin)
{
	_irq.fall(mbed::callback(this, &MPU9250::onInterrupt));
}

MbedMPU9250::~MbedMPU9250() {}

bool MbedMPU9250::_writeSingleByte(MPU9250::Register targetRegister, int value)
{
	char registerAndValue[2] = { (char)targetRegister, (char)value };
	return _i2c.write(_i2cAddress, registerAndValue, sizeof(registerAndValue)) == 0;
}

bool MbedMPU9250::_readSingleByte(MPU9250::Register targetRegister, int& value)
{
	char target = (char)targetRegister;
	char result = 0;
	const bool success = (_i2c.write(_i2cAddress, &target, 1, true) == 0)
		&& (_i2c.read(_i2cAddress, &result, 1) == 0);
	value = (int)result;
	return success;
}

bool MbedMPU9250::_readDoubleByte(MPU9250::Register startRegister, int& value)
{
	char targetRegister = (char)startRegister;
	uint16_t result = 0;
	bool success = (_i2c.write(_i2cAddress, &targetRegister, 1, true) == 0)
		&& (_i2c.read(_i2cAddress, (char*)&result, sizeof(result)) == 0);
	value = (int)result;
	return success;
}

void MbedMPU9250::_enableIRQ()
{
	_irq.enable_irq();
}

void MbedMPU9250::_disableIRQ()
{
	_irq.disable_irq();
}

void MbedMPU9250::_wait(int millis)
{
	rtos::ThisThread::sleep_for(std::chrono::milliseconds(millis));
}

#endif // __MBED__
