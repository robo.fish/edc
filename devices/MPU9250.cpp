#include "MPU9250.hpp"
#include <cmath>

using namespace robofish::edc;

#define MPU9250_USES_INTERRUPT (0)
#define MPU9250_USES_FIFO (0)

MPU9250::MPU9250(I2CAddress address) :
	_i2cAddress((int)address),
	gyroScale(GyroConfig::Flag::ScaleSelect_250)
{
	gyroSensitivity = GyroConfig::sensitivity(gyroScale);
}

MPU9250::~MPU9250()
{}

volatile bool newValuesReadyForReading = false;

/**
 * Interrupt handlers must finish their work quickly.
 * Accessing communication channels like I2C is not allowed.
 */
void MPU9250::onInterrupt()
{
	newValuesReadyForReading = true;
}

bool MPU9250::initialize()
{
	bool success = true;

	// waking up the device, clearing sleep flag (bit 6)
	success = success && _writeSingleByte(Register::PWR_MGMT_1, PowerManagement1::H_RESET);
	_wait(10);

	// disabling FSYNC and setting gyro sampling rate to 1 kHz
	success = success && _writeSingleByte(Register::CONFIG, 0x03);
	success = success && _writeSingleByte(Register::SMPLRT_DIV, 0x04);

	int gyroConfig = 0;
	if (_readSingleByte(Register::GYRO_CONFIG, gyroConfig))
	{
		gyroConfig &= ~0x03; // clears FCHOICE bits
		gyroConfig &= ~0x18; // clears GFS bits
		gyroConfig |= gyroScale;
		_writeSingleByte(Register::GYRO_CONFIG, gyroConfig);
	}

#if MPU9250_USES_FIFO
	success = success && _writeSingleByte(Register::FIFO_EN, FIFOConfig::GYRO_XOUT | FIFOConfig::TEMP_FIFO_EN);
#endif

#if MPU9250_USES_INTERRUPT
	success = success && _writeSingleByte(Register::INT_PIN_CFG, InterruptConfig::OPEN | InterruptConfig::LATCH_INT_EN | InterruptConfig::ACTL | InterruptConfig::BYPASS_EN);
	success = success && _writeSingleByte(Register::INT_ENABLE, InterruptStatus::RAW_DATA_RDY_INT);
#endif

	return success;
}

bool MPU9250::test()
{
	int whoami = 0x00;
	return _readSingleByte(Register::WHOAMI, whoami) && (whoami == (int)WhoAmI::MPU9250);
}

void MPU9250::_updateValues()
{
#if MPU9250_USES_INTERRUPT
	_disableIRQ();
	if (newValuesReadyForReading)
	{
		int interrupt_status; // reading the interrupt status register automatically clears it
		if (_readSingleByte(Register::INT_STATUS, interrupt_status) && ((interrupt_status & InterruptStatus::RAW_DATA_RDY_INT) != 0))
		{
			_readDoubleByte(Register::GYRO_XOUT_H, GYRO_OUT);
			_readDoubleByte(Register::TEMP_OUT_H, TEMP_OUT);
		}
		newValuesReadyForReading = false;
	}
	_enableIRQ();
#else
	_readDoubleByte(Register::GYRO_XOUT_H, GYRO_OUT);
	_readDoubleByte(Register::TEMP_OUT_H, TEMP_OUT);
#endif
}

void MPU9250::readValues(MPU9250::Values & values)
{
	_updateValues();

	values.gyroX = (int)round(float(GYRO_OUT) / gyroSensitivity);

	static const float sensitivity = 333.87f;
	static const float offset = 21.0f;
	values.temperature = (float(TEMP_OUT) / sensitivity) + offset;
}
